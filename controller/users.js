const User = require("../models/User");
const Course = require('../models/Course')
const bcrypt = require("bcrypt")
const dotenv = require ("dotenv");
let salt = parseInt(process.env.SALT);
const auth = require('../auth.js')

dotenv.config();

module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result
	})
}

module.exports.createUser = (requestBody) => {
	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password,salt),
		mobileNo: requestBody.mobileNo
	});
	return newUser.save().then((user,err) => {
		if(user){
			console.log (newUser)
			return user;
		} else {
			return 'Failed to Register account';
		};
	});
};

// User Authentication
/*
Steps:
1. Check the database if the user email exists
2. Compare if the password privided in the login form with password stored in the database.
3. Generate / return a JSON web token if the user is successfully logged in, and return false if not.
*/

module.exports.loginUser = (data) => {
	// findOne method returns the first record in the collection that matches the search criteria
	return User.findOne({email: data.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		} else {
			// User exists
			// "compareSync" method from the bcrypt used in comparing the non-encrypted password from the login and the database password. It returns "true" or "false" depending on the result
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
			// if the passwords match, return token
			if (isPasswordCorrect){
				return{accessToken: auth.createAccessToken(result.toObject())}
			}else{
				// passwords do not match
				return false;
			}
		}
	})
}

// Functionalities [RETRIEVE] the user details
/*
Steps:
1. Find the document in the database using the user's ID
2. Reassign the password of the returned document to an empty string
3. Return the result back to the client
*/
module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		// change the value of the user's password to an empty string
		result.password = '';
		return result;
	})
}

// Enroll registered user
/*
Enrollment Steps
	1. Look for the user by its ID
		-push the details of the course we're trying to enroll in. We'll push the details to a new enrollment subdocument in our user.
	2. Look for the course by its ID.
		-push the details of the enrollee/user who's trying to enroll
*/
module.exports.enroll = async (req, res) => {
		console.log("Test enroll route");
		console.log(req.user.id);//the user's id from the decoded token after verify()
		console.log(req.body.courseId); //the course ID from our request body

		// Process stops here and sends response if the user is an admin
		if(req.user.isAdmin) {
				return res.send({ message: "Action Forbidden"})
		}

		// get the user's ID to save the courseId inside the enrollments field
		let isUserUpdated = await User.findById(req.user.id).then(user => {
			// add the courseId in an object and push that object into user's enrollments array:
			let newEnrollment = {
				courseId: req.body.courseId
			}
			user.enrollments.push(newEnrollment);

			// save the changes made to our user document
			return user.save().then(user => true).catch(err => err.message)
			
			//if isUserUpdated does not contain the boolean true, we will stop our process and return a message to our client
			if (isUserUpdated !== true){
				return res.send({message: isUserUpdated})
			}
		});

		// Find the course ID that we will need to push to our enrollee
		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
			let enrollee = {userId:req.user.id}
			course.enrollees.push(enrollee);

			// save the course document
			return course.save().then(course => true).catch(err => err.message)
			if(isCourseUpdated !== true){return res.send ({message:isCourseUpdated})}
		})

		// send message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdated contain the boolean true
		if (isUserUpdated && isCourseUpdated){return res.send({message:"Enrolled Successfully"})}
}

// Get user's enrollments
module.exports.getEnrollments = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(err => res.send(err))
}

// module.exports.deleteTask = (taskId) => {
// 	return Task.findByIdAndRemove(taskId).then((fulfilled,rejected) => {
// 		if (fulfilled){
// 			return 'The task has been successfully removed'
// 		} else {
// 			return 'Failed to remove task'
// 		};
// 	})
// }

// module.exports.taskCompleted = (taskId) => {
// 	return Task.findById(taskId).then((found,error) => {
// 		if (found){
// 			console.log(found);
// 			found.status = 'Completed';
// 			return found.save().then((updatedTask,saveErr)=>{
// 				if(updatedTask){
// 					return 'Task has been successfully modified to Completed'
// 				} else {
// 					return saveErr;
// 				}
// 			});
// 		} else {
// 			return 'Error! No doc found'
// 		};	
// 	})
// }

// module.exports.taskPending = (taskId) => {
// 	return Task.findById(taskId).then((found,error) => {
// 		if (found){
// 			console.log(found);
// 			found.status = 'Pending';
// 			return found.save().then((updatedTask,saveErr)=>{
// 				if(updatedTask){
// 					return 'Task has been successfully modified to Pending'
// 				} else {
// 					return saveErr;
// 				}
// 			});
// 		} else {
// 			return 'Error! No doc found'
// 		};	
// 	})
// }

// module.exports.taskUpdateName = (taskId,updatedName) => {
// 	return Task.findById(taskId).then((found,error) => {
// 		if (found){
// 			console.log(found);
// 			originalName = found.name;
// 			found.name = updatedName;
// 			return found.save().then((updatedTask,saveErr)=>{
// 				if(updatedTask){
// 					return `Task ${originalName} has been successfully modified to ${updatedName}`
// 				} else {
// 					return saveErr;
// 				}
// 			});
// 		} else {
// 			return 'Error! No doc found'
// 		};	
// 	})
// }

// module.exports.getTask = (taskId) => {
// 	return Task.findById(taskId).then((found,error) => {
// 		if (found){
// 			console.log(found);
// 			return found;
// 		} else {
// 			return 'Error! No doc found'
// 		};	
// 	})
// }