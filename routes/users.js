const express = require("express");

const router = express.Router();

const userController = require ("../controller/users")

const auth = require('../auth')

router.post("/register", (req,res) => {
	userController.createUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/", (req,res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController))
})

// User Authentication(login)
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})

// GET the user's details
router.get("/details", auth.verify, (req,res) => {
	userController.getProfile(req.user.id).then(result => res.send(result));
})

//Enroll our registered Users
//only the verified user can enroll in a course
router.post('/enroll', auth.verify, userController.enroll);

// Get logged user's enrollments
router.get('/getEnrollments', auth.verify, userController.getEnrollments);

// router.delete("/:task", (req,res) => {
// 	console.log(req.params.task)
// 	let taskId = req.params.task;
// 	userController.deleteTask(taskId).then(resultFromController => res.send(resultFromController))
// })	

// router.put('/:task/completed', (req,res) =>{
// 	console.log(req.params.task);
// 	let taskId = req.params.task;
// 	userController.taskCompleted(taskId).then(resultFromController => res.send(resultFromController))
// })

// router.put('/:task/pending', (req,res) =>{
// 	console.log(req.params.task);
// 	let taskId = req.params.task;
// 	userController.taskPending(taskId).then(resultFromController => res.send(resultFromController))
// })

// router.get("/:task/id", (req,res) => {
// 	console.log(req.params.task);
// 	let taskId = req.params.task;
// 	userController.getTask(taskId).then(resultFromController => res.send(resultFromController))
// })

// router.put("/:task/update-name", (req,res) => {
// 	console.log(req.params.task);
// 	let taskId = req.params.task;
// 	let updatedName = req.body.name;
// 	userController.taskUpdateName(taskId,updatedName).then(resultFromController => res.send(resultFromController))
// })

module.exports = router;