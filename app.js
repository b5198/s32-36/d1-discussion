const express = require("express");
const mongoose = require("mongoose");
const dotenv = require ("dotenv");
const userRoute = require("./routes/users");
const courseRoute = require("./routes/courses")
const app = express();

dotenv.config();
// let favouriteFood = process.env.Food;
// console.log(favouriteFood);
let link = process.env.LINK;

const port = process.env.PORT;

// console.log(port)

mongoose.connect(link,{

    useNewUrlParser:true,
    useUnifiedTopology:true

});

const db = mongoose.connection;

// console.log(db);

db.on('error', console.error.bind(console,"Connection Error"));
db.once('open',() => console.log("Connected to Database"));

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use('/users',userRoute);
app.use('/courses',courseRoute);

app.get('/',(req,res) => {res.send('Welcome to Enrollment System!')})
app.listen(port,() => console.log(`Server running at port ${port}`));